#include "Passport.hpp"

namespace DigitalPassport {

	void Passport::addrecord(const account_name authority_account, const account_name user_account, uint64_t tag, uint64_t value) {
    	/*
			Add record describing user's skills and achievements.

			uint64_t value: from 0 to 100 mark from authority
		*/
	    require_auth(authority_account);
	    
	    authorityIndex authorities(_self, _self);
	    auto authority_itr = authorities.find(authority_account);
	    eosio_assert(authority_itr != authorities.end(), "Address for authority account not found");

	   	userIndex users(_self, _self);
	    auto user_itr = users.find(user_account);
	    eosio_assert(user_itr != users.end(), "Address for user account not found");

	    // check that 'value' is somewhere from 0 to 100.
	    eosio_assert(value <= 100, "Value is outbound, should be from 0 to 100");

	    recordIndex records(_self, _self);
	    // auto record_itr = records.get_index<N(bytag)>();
	    // eosio_assert(user_itr == users.end(), "Address for user account not found");
	    auto pk = records.available_primary_key();
	    auto nrecord = records.emplace(authority_account, [&](auto& record) {
	    	record.record_id = pk;
	        record.authority_account = authority_account;
	        record.user_account = user_account;
	        record.value = value;
	        record.tag = tag;
		});

	    // add pk to users record_ids vector
	    users.modify(user_itr, authority_account, [&](auto& user) {
	        user.record_ids.push_back(pk);
	    });

	    print_f("{\"id\": %, \"tag\": \"%\", \"value\": %}", pk, tag, value);
	}

	void Passport::getrecord(uint64_t record_id) {
		/*
    		Get record information.
    	*/
		recordIndex records(_self, _self);
	    auto itr = records.find(record_id);
	    eosio_assert(itr != records.end(), "Record for record_id is not found");
	    auto record = records.get(record_id);
	    print_f("{\"id\": %, \"user\": \"%\", \"auth\": \"%\", \"tag\": \"%\", \"value\": %}", 
	    	record.record_id, name{record.user_account}, name{record.authority_account}, record.tag, record.value);
	}

	void Passport::getrecbytag(uint64_t tag) {
		/*
			Retrieve all records with provided tag.
		*/
		recordIndex records(_self, _self);
		// todo find by tag
	    auto record_itr = records.get_index<N(bytag)>();
	    auto itr = record_itr.find(tag);
      	eosio_assert(itr != record_itr.end(), "Records not found.");

      	print("[");
      	// while ( record != record_itr.end() ) {
      	// 	if (tag == record.tag) {
       //      	print_f("{\"id\": %, \"tag\": \"%\", \"value\": %},", record.record_id, record.tag, record.value);
       //      }
       //      record = record_itr->next();
       //      if ( record != record_itr.end() ) {
       //      	print(",");
       //      }
      	// }

      	while( itr != record_itr.end() ) {
      		if ( tag == itr->tag ) {
      			print_f("{\"id\": %, \"user\": \"%\", \"auth\": \"%\", \"tag\": \"%\", \"value\": %},", 
      				itr->record_id, name{itr->user_account}, name{itr->authority_account}, itr->tag, itr->value);
      		}
      		itr++;
		}
        print("{}]");
	}

	void Passport::getrecbyuser(const account_name account) {
		/*
			Retrieve all records related to the user.
		*/
		recordIndex records(_self, _self);
		auto record_itr = records.get_index<N(byuser)>();
	    auto itr = record_itr.find(account);
      	eosio_assert(itr != record_itr.end(), "Records not found.");

      	print("[");
		while( itr != record_itr.end() ) {
      		if ( account == itr->user_account ) {
      			print_f("{\"id\": %, \"user\": \"%\", \"auth\": \"%\", \"tag\": \"%\", \"value\": %},", 
      				itr->record_id, name{itr->user_account}, name{itr->authority_account}, itr->tag, itr->value);
      		}
      		itr++;
		}
        print("{}]");
	}

	void Passport::updrecord(const account_name authority_account, uint64_t record_id, uint64_t tag, uint64_t value) {
		/*
			Update record.
		*/
	    require_auth(authority_account);
	    
	    authorityIndex authorities(_self, _self);
	    auto authority_itr = authorities.find(authority_account);
	    eosio_assert(authority_itr != authorities.end(), "Address for authority account not found");

	    recordIndex records(_self, _self);
	    auto itr = records.find(record_id);
	    eosio_assert(itr != records.end(), "Record for record_id is not found");

	    auto record = records.get(record_id);
	    eosio_assert(record.authority_account == authority_account, "Authority is not the owner of the record");

	    records.modify(itr, authority_account, [&](auto& record) {
	 		record.tag = tag;
	 		record.value = value;
		});
	    
	    print_f("{\"id\": %, \"user\": \"%\", \"auth\": \"%\", \"tag\": \"%\", \"value\": %}", 
	    	record.record_id, name{record.user_account}, name{record.authority_account}, record.tag, record.value);
	    // print(" | Name: ", new_record.name.c_str());
	}
}