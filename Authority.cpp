#include "Passport.hpp"

/*
	types:
	  0 - education institutes
	  1 - business 
	  2 - MOOC platforms, online education
	  3 - Hackathons, CTFs, olympiads, other comptetions
*/

namespace DigitalPassport {

	void Passport::addauthority(const account_name account, string& name, uint64_t weight, string& description, uint64_t type, string& link) {
    	/*
    		TODO who can add authorities?
    	*/
		require_auth(account);
	    authorityIndex authorities(_self, _self);
	    
	    auto iterator = authorities.find(account);
	    eosio_assert(iterator == authorities.end(), "Address for account already exists");

	    authorities.emplace(account, [&](auto& authority) {
	        authority.account_name = account;
	        authority.name = name;
	        authority.description = description;
	        authority.weight = 0;
	        authority.type = type;
	        authority.link = link;
		});
    }

    void Passport::getauthority(const account_name account) {
    	/*
			Get authority.
    	*/
    	authorityIndex authorities(_self, _self);
	    
	    auto iterator = authorities.find(account);
	    eosio_assert(iterator != authorities.end(), "Address for account not found");

	    auto authority = authorities.get(account);

		print_f("{\"name\": \"%\", \"weight\": %, \"description\": \"%\", \"type\": %, \"link\": \"%\"}", 
			authority.name.c_str(), authority.weight, authority.description.c_str(), authority.type, authority.link);
    }

    void Passport::updauthority(const account_name account, uint64_t weight, string& description, string& link) {
    	/*
			Update authority (weight, description, link fields can be updated).
    	*/
    	require_auth(account);

    	authorityIndex authorities(_self, _self);
	    auto itr = authorities.find(account);
	    eosio_assert(itr != authorities.end(), "Address for account not found");

	 	authorities.modify(itr, account, [&](auto& authority) {
	 		authority.weight = weight;
	 		authority.description = description;
	        authority.link = link;
		});
    }
}