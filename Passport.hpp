#include <eosiolib/eosio.hpp>
#include <eosiolib/print.hpp>
#include <eosiolib/transaction.h>
#include <string>

#define EDIT_INTERVAL 100

namespace DigitalPassport {
    using namespace eosio;
    using std::string;

    class Passport : public contract {
        using contract::contract;

        public:

            Passport(account_name self):contract(self) {}

            //@abi action
            void adduser(const account_name account, string& first_name, string& last_name, uint64_t birth_year);

            //@abi action
            void upduser(const account_name account, string& first_name, string& last_name, uint64_t birth_year);

            //@abi action
            void getuser(const account_name account);

            //@abi action
            void addauthority(const account_name account, string& name, uint64_t weight, string& description, uint64_t type, string& link);

            //@abi action
            void updauthority(const account_name account, uint64_t weight, string& description, string& link);

            //@abi action
            void getauthority(const account_name account);

        private:

            //@abi table record i64
            struct record {
                uint64_t record_id;
                uint64_t authority_account;
                // uint64_t authority_weight;
                uint64_t user_account;
                uint64_t value;
                // todo text
                uint64_t tag;
                // string meta_link;

                uint64_t primary_key() const { return record_id; }
                uint64_t get_tag() const { return tag; }
                account_name get_user() const {return user_account; }
                // uint64_t get_user() const { return user_account;}

                EOSLIB_SERIALIZE(record, (record_id)(authority_account)(user_account)(value)(tag))
            };

            //@abi table user i64
            struct user {
                uint64_t account_name;
                // uint64_t meta_link;
                string first_name;
                string last_name;
                uint64_t birth_year;
                vector<uint64_t> record_ids;
                // tapos_block_num
                uint64_t created;

                uint64_t primary_key() const { return account_name; }

                EOSLIB_SERIALIZE(user, (account_name)(first_name)(last_name)(birth_year)(record_ids)(created))
            };

            //@abi table record i64
            struct authority {
                uint64_t account_name;
                string name;
                string description;
                uint64_t weight;
                uint64_t type;
                string link;

                uint64_t primary_key() const { return account_name; }

                EOSLIB_SERIALIZE(authority, (account_name)(name)(description)(weight)(type)(link))
            };

            typedef multi_index<N(user), user> userIndex;
            typedef multi_index<N(authority), authority> authorityIndex;
            typedef multi_index<N(record), record, 
                indexed_by<N(bytag), const_mem_fun<record, uint64_t, &record::get_tag>>,
                indexed_by<N(byuser), const_mem_fun<record, uint64_t, &record::get_user>>
            > recordIndex;

        public:
            // CRUD record table

            //@abi action
            void addrecord(const account_name authority_account, const account_name user_account, uint64_t tag, uint64_t value);

            //@abi action
            void getrecord(uint64_t record_id);

            //@abi action
            void getrecbytag(uint64_t tag);

            //@abi action
            void getrecbyuser(const account_name account);

            //@abi action
            void updrecord(const account_name authority_account, uint64_t record_id, uint64_t tag, uint64_t value);
    };

    EOSIO_ABI(Passport, (adduser)(getuser)(upduser)(addauthority)(getauthority)(updauthority)(addrecord)(getrecord)(getrecbytag)(getrecbyuser)(updrecord));
}