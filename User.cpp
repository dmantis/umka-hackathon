#include "Passport.hpp"

namespace DigitalPassport {

    void Passport::adduser(const account_name account, string& first_name, string& last_name, uint64_t birth_year) {
    	/*
    		Add new user.
    	*/
		require_auth(account);
	    userIndex users(_self, _self);

	    auto iterator = users.find(account);
	    eosio_assert(iterator == users.end(), "Address for account already exists");

	    users.emplace(account, [&](auto& user) {
	        user.account_name = account;
	        user.first_name = first_name;
	        user.last_name = last_name;
	        user.birth_year = birth_year;
	        user.created = tapos_block_num();
		});
    }

    void Passport::upduser(const account_name account, string& first_name, string& last_name, uint64_t birth_year) {
    	/*
    		Update user fields.
    	*/
        require_auth(account);
	    userIndex users(_self, _self);

	    auto iterator = users.find(account);
	    eosio_assert(iterator != users.end(), "Address for account not found");
		uint64_t tbn = tapos_block_num();
		auto current_user = users.get(account);
		eosio_assert(tbn - current_user.created < EDIT_INTERVAL, "Time for update is end");

	 	users.modify(iterator, account, [&](auto& user) {
	 		user.first_name = first_name;
	 		user.last_name = last_name;
	        user.birth_year = birth_year;
		});
    }
        
    void Passport::getuser(const account_name account) {
    	/*
    		Get user information.
    	*/
	    userIndex users(_self, _self);

	    auto iterator = users.find(account);
	    eosio_assert(iterator != users.end(), "Address for account not found");

	    /**
	     * The "get" function returns a constant reference to the object
	     * containing the specified secondary key
	    */
	    auto user = users.get(account);
		print_f("{\"first_name\": \"%\", \"last_name\": \"%\", \"birth_year\": %, \"records\": %}", 
			user.first_name.c_str(), user.last_name.c_str(), user.birth_year, user.record_ids.size());
    }
}